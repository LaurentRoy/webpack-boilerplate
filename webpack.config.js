const path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
// const CleanWebpackPlugin = require('clean-webpack-plugin')



module.exports = (env, argv) => {
    devMode = argv.mode === 'development'

    let cssLoaders = [
        devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
        'css-loader'
    ]

    if(!devMode) cssLoaders.push({ 
        loader: 'postcss-loader', 
        options: { 
            ident: 'postcss',
            plugins: [
                require('autoprefixer')({browsers: ['last 2 version', 'ie > 8']})
            ]
        } 
    })

    cssLoaders.push('sass-loader') 

    let config = {
        entry: './src/main.js',
        output: {
            path: path.resolve(__dirname, './dist'),
            publicPath: '/assets/',
            filename: 'bundle.js'
        },
        module: {
            rules: [
                // {
                //     enforce: "pre",
                //     test: /\.js$/,
                //     exclude: /node_modules/,
                //     loader: "eslint-loader"
                // },
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: cssLoaders
                },
            ]
        },
        optimization: {
            minimizer: [new UglifyJsPlugin({
              sourceMap: true,
            })]
        },
        devtool: devMode ? 'eval-source-map' : false,
        plugins: [
            new MiniCssExtractPlugin({
                filename: devMode ? '[name].css' : '[name].css',
                chunkFilename: devMode ? '[id].css' : '[id].css'
            })
        ],
        devServer:{
            contentBase: path.resolve('./public'),
            overlay: true
        }
    }

    return config;
};